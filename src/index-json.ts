import fs from 'fs'
import type { Express, Request, Response } from 'express'
import express from 'express'
import dotenv from 'dotenv'

dotenv.config()

const app: Express = express()
const port = process.env.PORT

const path = './src/database/measures.json'

app.use(express.urlencoded())
app.use(express.json()) // if needed

interface Temperature {
  id: number
  value: number
  unit: 'celsius' | 'fahrenheit'
  type: 'temperature'
  name: string
}

const readJson = (path: string): Temperature[] => {
  const data = fs.readFileSync(path, 'utf8')
  const json = JSON.parse(data)

  return json
}

const addToJson = (path: string, data: any): void => {
  const json = readJson(path)
  let id = 1
  for (let i = 0; i < json.length; i++) {
    while (json[id])
      id++
  }
  id = id + 1
  data.id = id
  json.push(data)
  fs.writeFileSync(path, JSON.stringify(json))
}

const replaceJson = (path: string, json: Temperature[]): void => {
  fs.writeFileSync(path, JSON.stringify(json))
}

const removeInJson = (path: string, id: number) => {
  const arr = readJson(path)
  return arr.filter(el => el.id !== id)
}

app.get('/', (req: Request, res: Response) => {
  res.json('Express with TypeScript Server')
})

app.get('/measures', (req: Request, res: Response) => {
  if (!fs.existsSync(path))
    fs.writeFileSync(path, '[]')

  const json = readJson(path)

  res.json(json)
})

app.post('/measures', (req: Request, res: Response) => {
  const body = req.body
  addToJson(path, body)

  res.json({
    message: 'success',
    data: body,
  })
})

app.get('/measures/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const json = readJson(path)
  const data = json[id]

  res.json(data)
})

app.put('/measures/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  let body = req.body
  body = {
    id,
    ...body,
  }

  const json = readJson(path)

  json[id] = body
  replaceJson(path, json)

  res.json({
    message: 'success',
    data: body,
  })
})

app.delete('/measures/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  removeInJson(path, id)

  res.json({
    message: 'success',
    data: id,
  })
})

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`)
})

