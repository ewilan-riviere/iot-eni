import type { Express, Request, Response } from 'express'
import express from 'express'
import dotenv from 'dotenv'
import MongoApp from './class/MongoApp'
import type { Measure } from './class/Measure'

dotenv.config()

const app: Express = express()
const port = process.env.PORT

app.use(express.urlencoded())
app.use(express.json()) // if needed

let setupMA
const setup = async () => {
  setupMA = await MongoApp.create<Measure>('measures_list')
}
setup()

app.listen(port, () => {
  app.get('/measures', async (req: Request, res: Response) => {
    const data = await setupMA.findAll()

    res.json({
      data,
    })
  })

  app.get('/measures/clear', async (req: Request, res: Response) => {
    setupMA.clearAll()

    res.json({
      message: 'All data cleared',
    })
  })

  app.post('/measures', async (req: Request, res: Response) => {
    const measure = await setupMA.insert(req.body)

    res.json({
      message: 'success',
      data: measure,
    })
  })

  app.get('/measures/:id', async (req: Request, res: Response) => {
    const id = parseInt(req.params.id)
    const measure = await setupMA.findOne(id)

    res.json({
      message: 'success',
      data: measure,
    })
  })

  app.put('/measures/:id', async (req: Request, res: Response) => {
    const id = parseInt(req.params.id)
    let body = req.body
    body = {
      id,
      ...body,
    }

    setupMA.update(id, body)

    res.json({
      message: 'success',
      data: body,
    })
  })

  app.delete('/measures/:id', async (req: Request, res: Response) => {
    const id = parseInt(req.params.id)
    setupMA.delete(id)

    res.json({
      message: 'success',
      data: id,
    })
  })

  console.log(`⚡️[server]: Server is running at http://localhost:${port}`)
})

