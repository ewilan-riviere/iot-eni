type IMeasureUnit = 'celsius' | 'fahrenheit'
type IMeasureType = 'temperature'
interface IMeasure {
  id?: number
  value?: number
  unit?: IMeasureUnit
  type?: IMeasureType
  name?: string
}

export class Measure implements IMeasure {
  id?: number
  value: number
  unit: IMeasureUnit
  type: IMeasureType
  name: string

  constructor(id: number, value: number, unit: IMeasureUnit, type: IMeasureType, name: string) {
    this.id = id
    this.value = value
    this.unit = unit
    this.type = type
    this.name = name
  }
}
