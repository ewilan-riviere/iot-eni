import type { Collection, Db } from 'mongodb'
import { MongoClient } from 'mongodb'

const dbName = 'measures'
type dbCollect = 'measures_list'

export default class MongoApp<T extends { id?: number }> {
  collect: dbCollect
  client?: MongoClient
  db?: Db
  collection?: Collection<Document>

  constructor(collect: dbCollect) {
    this.collect = collect
  }

  static create = async <T>(collect: dbCollect) => {
    const ma = new MongoApp<T>(collect)
    await ma.setDb()
    ma.setCollection()

    return ma
  }

  private setDb = async () => {
    const url = 'mongodb://localhost:27017'
    this.client = new MongoClient(url)

    await this.client.connect()

    this.db = this.client.db(dbName)
  }

  private setCollection = () => {
    if (!this.db?.collection(this.collect))
      this.db?.createCollection(this.collect)

    this.collection = this.db?.collection(this.collect)
  }

  close = () => {
    this.client.close()
  }

  clearAll = () => {
    this.db?.dropCollection(this.collect)
    this.db?.createCollection(this.collect)

    this.collection = this.db?.collection(this.collect)
  }

  insert = async (raw: T) => {
    raw.id = await this.incrementId()
    const data = raw as unknown as Document
    this.collection?.insertOne(data)

    return raw
  }

  insertMany = (raw: T[]) => {
    const data = raw as unknown as Document[]
    this.collection?.insertMany(data)
  }

  findAll = async (): Promise<T[]> => {
    return await this.db.collection(this.collect)
      .find()
      .toArray() as unknown as T[]
  }

  count = () => this.collection?.countDocuments()

  findOne = async (id: number): Promise<T> => {
    return this.db.collection(this.collect)
      .findOne({ id }) as unknown as T
  }

  update = async (id: number, raw: T) => {
    const data = raw as unknown as Document
    return this.db.collection(this.collect)
      .updateOne({ id }, { $set: data })
  }

  delete = async (id: number) => {
    return this.db.collection(this.collect)
      .deleteOne({ id })
  }

  async incrementId() {
    let id = await this.count() + 1
    const exist = await this.findOne(id)
    if (exist !== null)
      id = exist.id + 1

    return id
  }
}
