const fs = require('fs')
const express = require('express')
const cors = require('cors')
const mqtt = require('mqtt')
const ip = '192.168.1.105'
const client = mqtt.connect(`mqtt://${ip}`)

const path = './src/database/mqtt.json'
let messages = []
const app = express()
app.use(cors({
  origin: '*',
}))

const readJson = (path) => {
  const data = fs.readFileSync(path, 'utf8')
  const json = JSON.parse(data)

  return json
}
const addToJson = (path, data) => {
  const json = readJson(path)
  let id = 1
  for (let i = 0; i < json.length; i++) {
    while (json[id])
      id++
  }
  id = id + 1
  data.id = id
  data.date = Date.now()
  json.push(data)
  fs.writeFileSync(path, JSON.stringify(json))
}

app.get('/', (req, res) => {
  const data = readJson(path).reverse()

  res.json(data)
  // res.json('express api')
})

const server = app.listen(3000, () => {
  console.log('server running on port 3000')
})

const io = require('socket.io')(server, {
  cors: {
    origin: '*',
    methods: ['GET', 'POST'],
  },
})

io.on('connection', (socket) => {
  // console.log(socket.id)
  socket.on('SEND_MESSAGE', (data) => {
    messages.push(data)
    io.emit('MESSAGE', messages)
  })
  socket.on('CLEAR_MESSAGE', () => {
    messages = []
    io.emit('MESSAGE', messages)
  })
  socket.on('LED', () => {
    console.log('EMIT LED')
    client.publish('control_led', '1')
  })
  socket.on('LED_OFF', () => {
    console.log('EMIT LED_OFF')
    client.publish('control_led', '0')
  })
})

client.subscribe('confort')

client.on('message', (topic, payload) => {
  console.log(`${topic} ${payload.toString()}`)
  addToJson(path, {
    topic,
    message: JSON.parse(payload.toString()),
  })
  io.emit('MESSAGE', messages)
  // client.end()
})
